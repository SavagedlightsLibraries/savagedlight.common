﻿/*
Savagedlight
Copyright (c) 2010-2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace Savagedlight
{
    public static class Misc
    {
        /// <summary>
        /// Retrieves an application-specific directory path for storing/retrieving temporary files
        /// </summary>
        public static string MyTemporaryDirectory
        {
            get {
                Assembly assembly = Assembly.GetEntryAssembly();
                if (assembly == null)
                {
                    assembly = Assembly.GetAssembly(typeof(Misc));
                }
                return Path.Combine(
                    Path.GetTempPath(),
                    assembly.GetName().Name);
            }
        }
        
        /// <summary>
        /// Get a DateTime object representing the local time defined by the provided unixtime
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public static DateTime Unixtime(Int64 unixTime)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return dt.AddSeconds(unixTime).ToLocalTime();
        }

        public static Random NewRandom()
        {
            RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();
            byte[] blah = new byte[4];
            random.GetNonZeroBytes(blah);
            int seed = BitConverter.ToInt32(blah, 0);
            return new Random(seed);
        }
    }
}
