﻿/*
Savagedlight
Copyright (c) 2010-2013 Demoder <demoder@demoder.me>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Savagedlight.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Converts first character to uppercase, and the remaining to lowercase. Special characters and numbers are passed as-is.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string UppercaseFirst(this string str)
        {
            string input = str.ToLowerInvariant();

            StringBuilder sb = new StringBuilder();
            bool first = true;
            foreach (char c in input)
            {
                if (first)
                {
                    sb.Append(c.ToString().ToUpperInvariant());
                    first = false;
                }
                else
                {
                    sb.Append(c.ToString().ToLowerInvariant());
                }
            }
            return sb.ToString();
        }
    }
}
